package unitpaylib

import (
	"encoding/json"
	"log"
)

type ApiResultGetCommisions struct {
	ApplePay  float64 `json:"applepay"`
	Megafon   float64 `json:"mf"`
	WebMoney  float64 `json:"webmoney"`
	Yandex    float64 `json:"yandex"`
	AlfaClick float64 `json:"alfaClick"`
	Svyaznoy  float64 `json:"svyaznoy"`
	PayPal    float64 `json:"paypal"`
	EuroSet   float64 `json:"euroset"`
	Tele2     float64 `json:"tele2"`
	Beeline   float64 `json:"beeline"`
	Mts       float64 `json:"mts"`
	Card      float64 `json:"card"`
	Qiwi      float64 `json:"qiwi"`
}

type ApiResponseGetComisions struct {
	Result *ApiResultGetCommisions `json:"result"`
	Error  *ApiError               `json:"error"`
}

func (o *Api) GetCommisions() *ApiResultGetCommisions {
	params := make(map[string]string)
	params["login"] = o.AccountEmail
	params["secretKey"] = o.AccountKey
	req, err := o.NewRequest("getCommissions", params)
	if err != nil {
		log.Println(err)
		return nil
	}

	code, body, err := o.Request(req)
	if err != nil {
		log.Println(err)
		return nil
	}
	if code != 200 {
		log.Printf("getCommisions api response %d\n%v", code, body)
		return nil
	}

	apiResult := ApiResponseGetComisions{}
	err = json.Unmarshal(body, &apiResult)
	if err != nil {
		log.Println(err)
		return nil
	}
	return apiResult.Result
}
