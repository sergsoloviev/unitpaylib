package unitpaylib

import (
	"io/ioutil"
	"net/http"
	"net/url"

	"log"
)

type Api struct {
	PublicKey    string
	SecretKey    string
	Url          *url.URL
	ProjectId    string
	AccountKey   string
	AccountEmail string
}

func (o *Api) SetPublicKey(key string) (bool, error) {
	o.PublicKey = key
	// checks
	return true, nil
}

func (o *Api) SetSecretKey(key string) (bool, error) {
	o.SecretKey = key
	// checks
	return true, nil
}

func (o *Api) SetUrl(urlString string) (bool, error) {
	u, err := url.Parse(urlString)
	if err != nil {
		return false, err
	}
	o.Url = u
	return true, nil
}

func (o *Api) SetProjectId(id string) (bool, error) {
	o.ProjectId = id
	return true, nil
}

func (o *Api) SetAccountKey(key string) (bool, error) {
	o.AccountKey = key
	// checks
	return true, nil
}

func (o *Api) SetAccountEmail(v string) (bool, error) {
	o.AccountEmail = v
	// checks
	return true, nil
}

func (o *Api) NewRequest(method string, params map[string]string) (*http.Request, error) {
	req, err := http.NewRequest("GET", o.Url.String(), nil)
	req.Header.Add("Accept", "application/json;charset=UTF-8")
	req.Header.Add("Accept-Charset", "UTF-8")
	req.Header.Add("Accept-Encoding", "UTF-8")
	q := req.URL.Query()
	q.Add("method", method)
	q.Add("params[secretKey]", o.SecretKey)
	q.Add("params[projectId]", o.ProjectId)
	for k, v := range params {
		q.Set("params["+k+"]", v)
	}
	req.URL.RawQuery = q.Encode()
	return req, err
}

func (o *Api) Request(req *http.Request) (int, []byte, error) {
	client := &http.Client{}
	response, err := client.Do(req)
	if err != nil {
		log.Println(err)
		return 0, nil, err
	}
	resp_body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Println(err)
		return 0, nil, err
	}
	response.Body.Close()
	return response.StatusCode, resp_body, nil
}

type ApiError struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}
