package unitpaylib

import (
	"log"

	"encoding/json"
)

type ApiResultInitPayment struct {
	Type        string `json:"type"`
	Message     string `json:"message"`
	PaymentId   int    `json:"paymentId"`
	RedirectUrl string `json:"redirectUrl"`
	ReceiptUrl  string `json:"receiptUrl"`
	StatusUrl   string `json:"statusUrl"`
}

type ApiResponseInitPayment struct {
	Result *ApiResultInitPayment `json:"result"`
	Error  *ApiError             `json:"error"`
}

func (o *Api) InitPayment(params map[string]string) (apiResponse *ApiResponseInitPayment) {
	req, err := o.NewRequest("initPayment", params)
	if err != nil {
		log.Println(err)
		return nil
	}

	code, body, err := o.Request(req)
	if err != nil {
		log.Println(err)
		return nil
	}
	if code != 200 {
		log.Printf("api response %d\n%v", code, body)
		return nil
	}

	err = json.Unmarshal(body, &apiResponse)
	if err != nil {
		log.Println(err)
		return nil
	}
	return apiResponse
}
