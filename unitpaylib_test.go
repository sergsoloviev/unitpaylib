package unitpaylib

import (
	"testing"
)

func TestApi_SetPublicKey(t *testing.T) {
	api := Api{}
	testKey := "test_key-1234"

	api.SetPublicKey(testKey)
	if api.PublicKey != testKey {
		t.Errorf("Incorrect test key, got: %s, want: %s.", api.PublicKey, testKey)
	}
}

func TestApi_SetUrl(t *testing.T) {
	api := Api{}
	testUrl := "https://unitpay.ru/api"
	api.SetUrl(testUrl)
	if api.Url == nil {
		t.Errorf("Incorrect url, got: %v.", api.Url)
	}
}
